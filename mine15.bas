#include <zx0.bas>
REM MINESWEEPER

POKE UINTEGER 23675, @graphicsbank
DIM grid(1 to 225) AS ubyte
DIM xlist(0 to 225) AS ubyte
DIM ylist(0 to 225) AS ubyte
DIM flaggrid(1 to 225) AS ubyte
dim number, row, where , bnum, a, pitch, score as ubyte
dim difficulty, flagno, x, y, x1, y1, y2, x2, loc , listpointer as ubyte
flash 1 : ink 2 : paper 0
print at 10,9; "PRESS ANY KEY."
pause 0
flash 0
let leftkey = "a"
let rightkey = "d"
let upkey = "w"
let downkey = "s"
let look = " "
let newg = "n"
let flag = "m"

randomize
start()


'Subs and functions'

sub movementloop()
move:
if score = 0 then over 0 : ink 4: paper 0 : print at 0,0;"          \d  WIN!  \d            " : pause 0

over 1 : ink 5 : paper 0
print at x,y;" "
ink 0 : paper 7 ': bright 1
'if grid(loc) =0 and flaggrid(loc) = 2 then bright 0 : end if




IF INKEY = upkey then
press() 
if x > 3 then let x = x -1 :end if
end if 

IF INKEY = downkey then 
press()
if x < 17 then let x = x +1 : end if 
end if

IF INKEY = leftkey then
press()
if y > 8 then let y = y -1 :end if 
end if 
IF INKEY = rightkey then
press()
if y < 22 then
let y = y +1
end if 
end if 
IF INKEY = look then
dig()
over 0
print at 12, 0; "S:" ; score; "  "
over 1
end if
'flag'
IF INKEY = flag then
getloc()
delay(10)
BEEP .003,33
if flaggrid(loc) <> 2 then
if flaggrid(loc) = 0 then
print at x,y;"\c"
let flagno = flagno + 1
let flaggrid(loc) = 1
else
print at x,y;"\c"
let flagno = flagno - 1
let flaggrid(loc) = 0
end if
end if
over 0:ink 4:paper 0
print at 11, 0; "\c:" ; flagno 
over 1
end if
'new game' 
IF INKEY = newg then BEEP .003,33 : delay(20) : start() : end if 

'IF INKEY = "b" then BEEP .003,33 : delay(10) : boom() : end if 'debug option'

go to move

'movementloop()

end sub

sub start()
paper 0 : border 1 : ink 5 : over 0
dzx0Standard(@MSgame, 16384)
print at 0,0;"R = new keys. Select skill (2-5)"
let number = 15
let x = 10
let difficulty= 2
let y = 10
let loc = 108
let row = 1
let where = 1
let bnum = 0
let flagno = 0
let a = 1
let pitch = 225
let score = pitch
listpointer = 1

cleararrays()
startmenu()
print at 0,0;" Difficulty : "; difficulty; ". " ; newg;" for new game "
placebombs()
let score = score - bnum
findfriends()
drawstart()
amisafe() ' cursor starts in safe location - probably or it could crash, hard to test lol
movementloop()
end sub

sub startmenu()
keys:
'pause 0
if inkey = "r" then redefinekeys() : go to keys : end if
let difficulty = val(inkey)
if difficulty < 2 then go to keys : end if
if difficulty > 5 then go to keys : end if


end sub 


sub dig()

getloc()
delay(10)
BEEP .003,33
if flaggrid(loc) = 0 then

if grid(loc) = 9 then 
let flaggrid(loc) = 2
boom()
print at x,y;"\e" 
end if

if flaggrid(loc) <> 2 then
  if grid(loc) = 0 then
  bright 0
  print at x,y;"\b"  
  let score = score - 1
  let flaggrid(loc) = 2
  getfriends()
  printzeros()
  end if 
getloc()
if grid(loc) > 0 then
bright 1
let score = score - 1
print at x,y; grid(loc)
let flaggrid(loc) = 2
end if
end if
end if
end sub



sub printzeros()
let listpointer = listpointer -1
while listpointer > 0
let x1 = xlist(listpointer)
let y1 = ylist(listpointer)
let loc = (x1*15)+y1
if grid(loc) > 0 then 'ie the location is touching a bomb'
ink 0 : paper 7 : bright 1 : over 1
print at x1+3,y1+7; grid(loc)
let score = score - 1
else
bright 0
print at x1+3,y1+7;"\b"
let score = score - 1
getfriends()
end if
let listpointer = listpointer - 1
if listpointer <5 then pause 2 : end if 

wend
let listpointer = 1
over 0
end sub

sub getfriends()
if y1 < 15 then 
let y2 = y1+1 
let x2 = x1 
updatechecklist()
end if 
if y1 > 1 then 
let y2 = y1-1 
let x2 = x1
updatechecklist()
end if
if x1 < 14 then 
let x2 = x1+1
let y2 = y1
updatechecklist() 
end if 
if x1 > 0 then 
let x2 = x1-1
let y2 = y1
updatechecklist()
end if
if y1 >1 and x1 <14 then
let y2 = y1-1
let x2 = x1+1
updatechecklist()
end if
if y1 <15 and x1 <14 then
let y2 = y1+1
let x2 = x1+1
updatechecklist()
end if
if y1<15 and x1 > 0 then
let y2 = y1+1
let x2 = x1-1
updatechecklist()
end if
if y1 >1 and x1 > 0 then
let y2 = y1-1
let x2 = x1-1
updatechecklist()
end if
end sub

sub addnextdoor()
let loc = (x2*15)+y2
if grid(loc) <> 9 then let grid(loc) = grid(loc) +1 : end if
xyreset()
end sub

sub findfriends()
let x1=0
let y1 =1
for x1 = 0 to 14
For y1 = 1 to 15
let loc = (x1*15)+y1
If grid(loc) = 9 then
if y1 <15 then 
let y2 = y1+1  
let x2 = x1
addnextdoor()
end if 
if y1 > 1 then 
let y2 = y1-1 
let x2 = x1
addnextdoor()
end if
if x1 < 14 then 
let x2 = x1+1
let y2 = y1
addnextdoor()
end if 
if x1 > 0 then 
let x2 = x1-1 
let y2 = y1
addnextdoor() 
end if
if y1 >1 and x1 <14 then ' this works'
let y2 = y1-1
let x2 = x1+1
addnextdoor()
end if
if y1 <15 and x1 <14 then ' this works'
let y2 = y1+1
let x2 = x1+1
addnextdoor()
end if
if y1<15 and x1 > 0 then
let y2 = y1+1
let x2 = x1-1
addnextdoor()
end if
if y1 >1 and x1 > 0 then
let y2 = y1-1
let x2 = x1-1
addnextdoor()
end if
end if
next
next
end sub

sub xyreset()
let y2 = y1
let x2 = x1
end sub

sub getloc()
let y1 = y-7
let x1 = x-3
let loc = (x1*15)+y1
end sub

sub cleararrays()
For a = 1 to pitch
let grid(a) = 0
let flaggrid(a) = 0
let xlist(a-1) = 0
let xlist(a-1) = 0
next a
end sub

sub updatechecklist()
let loc = (x2*15)+y2
if flaggrid(loc) = 0 then ' check if location is virgin'
if grid(loc) < 9 then ' not a bomb'
let xlist(listpointer) = x2
let ylist(listpointer) = y2
let flaggrid(loc) = 2
let listpointer = listpointer + 1 
end if
end if  
xyreset()
end sub

sub amisafe()
'ensures player starts on a safe square'
let x = 4
let y = 8
getloc()
while grid(loc)<> 0
if y <> 15 then 
let y=y+1  
getloc()
else
let x=x+1  
let y=8  
getloc()
end if 
wend
if loc > 225 then let x = 10 : let y = 10 : end if
getloc()
end sub




sub amisafe2()
'ensures player starts on a safe square'
getloc()
while grid(loc)= 9
if y <> 15 then 
let y=y+1  
getloc()
else
let x=x+1  
let y=1  
getloc()
end if 
wend
end sub

sub press()
delay(10)
BEEP .003,33
getloc()

'if nothing is discovered then'
if flaggrid(loc) <2 then 
bright 1
end if
if flaggrid(loc) = 2 and grid(loc) = 0 then
bright 0
else
bright 1
end if
' if its a flag give it the right ink'
if flaggrid(loc) =1 then
ink 1
print at x,y;" "
else 
ink 0 
print at x,y;" "
end if
end sub

sub drawstart()
paper 7 : ink 0
bright 1
for b = 1 to number
for a = 1 to number
print at row+2,a+7;"\b"
next a
let row = row + 1
next b 
ink 5:paper 0
print at 10, 0; "\a:" ; bnum 
print at 11, 0; "\c:" ; flagno 
'print at 12, 0; "S:" ; score

end sub

sub placebombs()
For a = 1 to pitch
if INT(RND * 10)+1 < difficulty then 
let grid(a)= 9
let bnum = bnum + 1
End If
next a
end sub

sub boom()
over 0 
let row = 1
let where = 1
paper 7 : ink 0
for b = 1 to number
for a = 1 to number
bright 1
if flaggrid(where) = 1 then ink 2 : end if
print at row+2,a+7;grid(where)
ink 0
if flaggrid(where) = 1 then ink 4 : end if
if grid(where) = 9 then print at row+2,a+7;"\a" : end if 
ink 0
bright 0
if flaggrid(where) = 1 then paper 2 : end if
if grid(where) = 0 then print at row+2,a+7;"\b" : end if 
paper 7
let where = where + 1
next a
let row = row + 1
next b 
over 0

end sub

sub redefinekeys: ' redefine keys sub'
Paper 0: INK 5
pause 5
delay(10)
PRINT AT 4,9 ; "LEFT... "
pause 0
let leftkey = inkey
BEEP .003,33 
PRINT AT 5,9 ;  "RIGHT..."
pause 0
let rightkey = inkey
BEEP .003,33 
PRINT AT 6,9 ;  "UP...   " 
pause 0
let upkey = inkey
BEEP .003,33 
PRINT AT 7,9 ; "DOWN... " 
pause 0
let downkey = inkey
BEEP .003,33
PRINT AT 8,9 ; "DIG...  " 
pause 0
let look = inkey
BEEP .003,33
PRINT AT 9,9 ; "FLAG... " 
pause 0
let flag = inkey
BEEP .003,33
PRINT AT 10,9 ; "QUIT... " 
pause 0
let newg = inkey
BEEP .003,33
PRINT AT 11,9 ; "DONE!   "
start()
end sub


MSgame:
    asm
       incbin "MSgame.scr.zx0"
    end asm

function fastcall delay (delay as ubyte) as ubyte
ASM 
ld b,a ; length of delay.
delay0: halt ; wait for an interrupt.
 djnz delay0 ; loop.
 ret 
END ASM
END FUNCTION

graphicsbank:
ASM

defb 092H, 07CH, 05CH, 0FEH, 07CH, 07CH, 092H, 000H ; a alt bomb 
defb 001H, 001H, 001H, 001H, 001H, 001H, 001H, 0FFH ; b pressed bomb
defb 000H, 01EH, 01EH, 010H, 010H, 010H, 07EH, 000H ; c flag
defb 03CH, 07EH, 0DBH, 0DBH, 0FFH, 081H, 042H, 03CH ; D big smile
defb 03CH, 07EH, 0DBH, 0DBH, 0FFH, 0E7H, 05AH, 03CH ; e sad face

End ASM 

