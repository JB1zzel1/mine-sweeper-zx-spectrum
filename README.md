# Mine Sweeper ZX Spectrum 

A version of Minesweeper written in ZX BASIC (Boriel)

keys are:

wsad m and space - n starts a new game. 

Keys are refinable by pressing r in the menu.

If there is one, the game will start you on an empty space that isn't touching any bombs.


## Installation

Compile it with something like: 

 -taB mystuff\mine15.bas



## Authors and acknowledgment
Made using ZX Basic, https://www.boriel.com/pages/the-zx-basic-compiler.html

ZX0,  https://github.com/einar-saukas/ZX0

ZX Paint brush

## License


